<html>
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<body>
	


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
<div class="row">

<div class="col-md-8">

<h1><a href="#" target="_blank"><img src="logo.png" width="80px"/>Ajax File Uploading with Database MySql</a></h1>
<hr> 

<form id="form" action="include.php" method="post" enctype="multipart/form-data">
<div class="form-group">
<label for="name">NAME</label>
<input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required />
</div>
<div class="form-group">
<label for="email">EMAIL</label>
<input type="text" class="form-control" id="email" name="email" placeholder="Enter email" required />
</div>
<input class="btn btn-success" type="submit" value="Upload">
</form>

</div>
</div>
</div>
</body>
<script type="text/javascript">

	$(document).ready(function (e) {
 	$("#form").on('submit',(function(e) {
  	e.preventDefault();
	  $.ajax({
		url		: "include.php",
	        type		: "POST",
	        data		:  new FormData(this),
	        contentType	:  false,
		cache		:  false,
	   	processData	:  false,
	  
	   success: function(data)
	      {
	   	console.log(data);
	      },
	     error: function(e) 
	      {
	       $("#err").html(e).fadeIn();
	      }          
	    });
	 }));
	});

</script>
</html>

